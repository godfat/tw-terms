
require 'yaml'

filename = ARGV.first

File.write(filename, YAML.dump(Hash[YAML.load(File.read(filename)).sort]))
