
require 'yaml'

filename = ARGV.first
map = Hash[YAML.load(File.read(filename)).sort]
a, b = map.keys.max_by(&:size).size, map.values.max_by(&:size).size * 2

printf("| %-#{a}s | %-#{b}s |\n", 'en', 'zh_TW')
puts("| #{'-'*(a)} | #{'-'*(b)} |")

map.each do |key, value|
  n = ((b / 2) - value.size) * 2 + value.size
  printf("| %-#{a}s | %-#{n}s |\n", key, value)
end
